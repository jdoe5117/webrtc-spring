package com.example.webrtctest;

import com.example.webrtctest.dtos.messages.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SocketIntegrationTests {

    private static final String roomId = "123";
    @LocalServerPort
    private int port;
    @Autowired
    private RoomsRepository roomsRepository;
    private SockJsClient sockJsClient;

    private WebSocketStompClient stompClient;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(roomsRepository, "rooms", new ConcurrentHashMap<String, Set<String>>());
        List<Transport> transports = new ArrayList<>();
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));

        this.sockJsClient = new SockJsClient(transports);

        this.stompClient = new WebSocketStompClient(sockJsClient);
        this.stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        this.stompClient.setTaskScheduler(new ThreadPoolTaskScheduler());
    }

    /*
     *  После подключения пользователь должен получить сообщение UsersInRoomChangeSocketMessage с одной сессией
     */
    @Test
    public void usersInRoomChangeSingleConnectionTest() throws Exception {

        final CountDownLatch allRequestsFinished = new CountDownLatch(1);
        final AtomicReference<Throwable> failure = new AtomicReference<>();

        TestSessionHandler handler = new TestSessionHandler(failure) {
            @Override
            public void afterConnected(final StompSession session, StompHeaders connectedHeaders) {
                session.subscribe("/user/socket-room/" + roomId, new StompFrameHandler() {
                    @Override
                    public Type getPayloadType(StompHeaders headers) {
                        return UsersInRoomChangeSocketMessage.class;
                    }

                    @Override
                    public void handleFrame(StompHeaders headers, Object payload) {
                        try {
                            UsersInRoomChangeSocketMessage msg = (UsersInRoomChangeSocketMessage) payload;
                            assertEquals(1, msg.getUsers().size());
                        } catch (Throwable t) {
                            failure.set(t);
                        } finally {
                            session.disconnect();
                            allRequestsFinished.countDown();
                        }
                    }
                });
            }
        };

        this.stompClient.connect("ws://localhost:{port}/socket-endpoint", new WebSocketHttpHeaders(), handler, this.port);

        if (allRequestsFinished.await(5, TimeUnit.SECONDS)) {
            if (failure.get() != null) {
                throw new AssertionError("usersInRoomChangeSingleConnectionTest failed", failure.get());
            }
        } else {
            fail("usersInRoomChangeSingleConnectionTest failed");
        }
    }

    /*
     * Тест эмулирующий все этапы обмена сообщениями между клиентами
     * Алгоритм следующий:
     * 1. Оба клиента устанавливают подключение
     * 2. Когда первый клиент получит UsersInRoomChangeSocketMessage содержащий две сесии он отправляет SDPOfferSocketMessage
     * 3. Второй клиент при получении SDPOfferSocketMessage отвечает OfferAnswerSocketMessage и ICECandidatesSocketMessage
     * 4. Первый клиент получает ICECandidatesSocketMessage и OfferAnswerSocketMessage и отправляет свой ICECandidatesSocketMessage
     * 5. Второй клиент получает ICECandidatesSocketMessage
     *
     * Тест считается успешно завершенным если оба клиента получили ICECandidatesSocketMessage в течении 10 секунд с момента создания подключений
     *
     */
    @Test
    public void allMessagesExchangeTest() throws Exception {
        final CountDownLatch iceCandidatesReceived = new CountDownLatch(2);
        final AtomicReference<Throwable> failure = new AtomicReference<>();
        final AtomicReference<Throwable> failure2 = new AtomicReference<>();

        TestSessionHandler handler1 = new TestSessionHandler(failure) {
            @Override
            public void afterConnected(final StompSession session, StompHeaders connectedHeaders) {
                session.subscribe("/user/socket-room/" + roomId, new StompFrameHandler() {
                    @Override
                    public Type getPayloadType(StompHeaders headers) {
                        return AbstractSocketMessage.class;
                    }

                    @Override
                    public void handleFrame(StompHeaders headers, Object payload) {
                        try {
                            AbstractSocketMessage msg = (AbstractSocketMessage) payload;
                            switch (msg.getMessageType()) {
                                case USERS_IN_ROOM_CHANGE:
                                    //когда появляется второй пользователь бросаем SDP Offer
                                    if (((UsersInRoomChangeSocketMessage) msg).getUsers().size() == 2) {
                                        session.send("/socket/message-consumer", new SDPOfferSocketMessage(null));
                                    }
                                    break;
                                //при получении ответа отправляем ICECandidates
                                case OFFER_ANSWER:
                                    session.send("/socket/message-consumer", new ICECandidatesSocketMessage(null));
                                    break;
                                case ICE_CANDIDATES:
                                    iceCandidatesReceived.countDown();
                                    break;
                            }
                        } catch (Throwable t) {
                            failure.set(t);
                        }
                    }
                });
            }
        };

        TestSessionHandler handler2 = new TestSessionHandler(failure2) {
            @Override
            public void afterConnected(final StompSession session, StompHeaders connectedHeaders) {
                session.subscribe("/user/socket-room/" + roomId, new StompFrameHandler() {
                    @Override
                    public Type getPayloadType(StompHeaders headers) {
                        return AbstractSocketMessage.class;
                    }

                    @Override
                    public void handleFrame(StompHeaders headers, Object payload) {
                        try {
                            AbstractSocketMessage msg = (AbstractSocketMessage) payload;
                            switch (msg.getMessageType()) {
                                case SDP_OFFER:
                                    session.send("/socket/message-consumer", new OfferAnswerSocketMessage(null));
                                    session.send("/socket/message-consumer", new ICECandidatesSocketMessage(null));
                                    break;
                                case ICE_CANDIDATES:
                                    iceCandidatesReceived.countDown();
                                    break;
                            }
                        } catch (Throwable t) {
                            failure.set(t);
                        }
                    }
                });
            }
        };

        this.stompClient.connect("ws://localhost:{port}/socket-endpoint", new WebSocketHttpHeaders(), handler1, this.port);
        this.stompClient.connect("ws://localhost:{port}/socket-endpoint", new WebSocketHttpHeaders(), handler2, this.port);

        if (iceCandidatesReceived.await(10, TimeUnit.SECONDS)) {
            if (failure.get() != null) {
                throw new AssertionError("allMessagesExchangeTest failed", failure.get());
            }
            if (failure2.get() != null) {
                throw new AssertionError("allMessagesExchangeTest failed", failure2.get());
            }
        } else {
            fail("allMessagesExchangeTest failed");
        }
    }

    private class TestSessionHandler extends StompSessionHandlerAdapter {

        protected final AtomicReference<Throwable> failure;

        public TestSessionHandler(AtomicReference<Throwable> failure) {
            this.failure = failure;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            this.failure.set(new Exception(headers.toString()));
        }

        @Override
        public void handleException(StompSession s, StompCommand c, StompHeaders h, byte[] p, Throwable ex) {
            this.failure.set(ex);
        }

        @Override
        public void handleTransportError(StompSession session, Throwable ex) {
            this.failure.set(ex);
        }
    }
}