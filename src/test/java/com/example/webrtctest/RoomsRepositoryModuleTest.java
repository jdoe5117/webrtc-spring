package com.example.webrtctest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomsRepositoryModuleTest {

    @Autowired
    private RoomsRepository roomsRepository;

    @Before
    public void clearState() {
        ReflectionTestUtils.setField(roomsRepository, "listeners", new ArrayList<RoomsRepoListener>());
        ReflectionTestUtils.setField(roomsRepository, "rooms", new ConcurrentHashMap<String, Set<String>>());
    }

    @Test
    public void testRepository() {

        roomsRepository.newSubscription("room1", "session1");
        assertEquals(1, roomsRepository.getRoomUsers("room1").size());
        assertEquals(new HashSet<>(Collections.singletonList("session1")), roomsRepository.getRoomUsers("room1"));

        roomsRepository.newSubscription("room1", "session2");
        assertEquals(2, roomsRepository.getRoomUsers("room1").size());
        assertEquals(new HashSet<>(Arrays.asList("session1", "session2")), roomsRepository.getRoomUsers("room1"));

        roomsRepository.unsubscribe("room1", "session1");
        assertEquals(1, roomsRepository.getRoomUsers("room1").size());
        assertEquals(new HashSet<>(Collections.singletonList("session2")), roomsRepository.getRoomUsers("room1"));

        roomsRepository.newSubscription("room2", "session2");
        assertEquals(1, roomsRepository.getRoomUsers("room1").size());
        assertEquals(new HashSet<>(Collections.singletonList("session2")), roomsRepository.getRoomUsers("room1"));
        assertEquals(1, roomsRepository.getRoomUsers("room2").size());
        assertEquals(new HashSet<>(Collections.singletonList("session2")), roomsRepository.getRoomUsers("room2"));

        roomsRepository.unsubscribe("room1", "session2");
        assertEquals(0, roomsRepository.getRoomUsers("room1").size());
        assertEquals(new HashSet<>(Collections.emptyList()), roomsRepository.getRoomUsers("room1"));
        assertEquals(1, roomsRepository.getRoomUsers("room2").size());
        assertEquals(new HashSet<>(Collections.singletonList("session2")), roomsRepository.getRoomUsers("room2"));
    }

}
