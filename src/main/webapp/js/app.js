var callButton = document.getElementById('callButton');
var hangupButton = document.getElementById('hangupButton');
var addStreamButton = document.getElementById('addStream');
var removeStreamsButton = document.getElementById('removeStreams');
var usersList = document.getElementById('users-list');
var roomId = document.getElementById('roomId').value;
var tracksCount = 0;

var addTrack = true;
var offerToReceive = true;

addStreamButton.onclick = addStream;
callButton.onclick = call;
hangupButton.onclick = hangupAndNotify;
removeStreamsButton.onclick = removeStreams;

var stompClient = null;
var stream;
var pc;
var iceCandidates = [];

var configuration = {
    iceServers: [
        {urls: "stun:stun.l.google.com:19302"},
        {urls: "stun:stun3.l.google.com:19302"}
    ]
};

window.onload = function () {
    connectSocket();
    getUserMedia();
};

function removeStreams() {
    pc.getSenders().forEach(function (sender) {
        pc.removeTrack(sender);
    });
}

function addStream() {
    navigator.mediaDevices.getUserMedia({
        audio: true,
        video: {
            width: {exact: 320},
            height: {exact: 240}
        }
    }).then(function (stream) {
        stream.getTracks().forEach(function (track) {
            pc.addTrack(track, stream);
        });
    }).catch(function (error) {
        console.log(error);
    });
}

function connectSocket() {
    var socket = new SockJS('/socket-endpoint');
    stompClient = Stomp.over(socket);
    stompClient.debug = null;
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/user/socket-room/' + roomId, handleSocketMessage);
    });
}

function getUserMedia() {
    navigator.mediaDevices.getUserMedia({
        audio: true,
        video: {
            width: {exact: 1280},
            height: {exact: 720}
        }
    }).then(function (s) {
        stream = s;
        yourvid.srcObject = stream;
    }).catch(function (error) {
        console.log(error);
    });
}

window.onbeforeunload = function () {
    hangup();
};

function call() {
    callButton.disabled = true;
    hangupButton.disabled = false;
    initPeerConnection();
    sendOffer();
}

function recvonly() {
    addTrack = false;
    offerToReceive = true;
}

function empty() {
    addTrack = false;
    offerToReceive = false;
}

function sendrecv() {
    addTrack = true;
    offerToReceive = true;
}

function sendonly() {
    addTrack = true;
    offerToReceive = false;
}

function initPeerConnection() {
    if (pc == null) {
        pc = new RTCPeerConnection(configuration);

        if (addTrack) {
            stream.getTracks().forEach(function (track) {
                pc.addTrack(track, stream);
            });
        }

        iceCandidates = [];


        pc.onicecandidate = function (e) {
            if (e.candidate) {
                iceCandidates.push({
                    label: e.candidate.sdpMLineIndex,
                    id: e.candidate.sdpMid,
                    candidate: e.candidate.candidate
                })
            }
        };

        pc.onicegatheringstatechange = function () {
            if (pc.iceGatheringState == 'complete') {
                sendMessage({
                    messageType: 'ICE_CANDIDATES',
                    data: iceCandidates
                });
            }
        };

        pc.ontrack = function (e) {
            console.log("got track")

            if (tracksCount == 0 || tracksCount == 1) {
                partnervid0.srcObject = e.streams[0];
            }

            if (tracksCount == 2 || tracksCount == 3) {
                partnervid1.srcObject = e.streams[0];
            }

            if (tracksCount == 4 || tracksCount == 5) {
                partnervid2.srcObject = e.streams[0];
            }

            tracksCount++;
        };

        pc.onnegotiationneeded = function () {
            console.log("onnegotiationneeded")
        }
    }
}

function sendOffer() {
    pc.createOffer(function (description) {
        // description.sdp = description.sdp.replace("a=group:BUNDLE audio video\r\n","");
        console.log("offer");
        console.log(description.sdp);

        pc.setLocalDescription(description);

        sendMessage({
            messageType: 'SDP_OFFER',
            data: description
        })
    }, function (error) {
        console.log(error)
    }, {
        'mandatory': {
            'OfferToReceiveAudio': offerToReceive,
            'OfferToReceiveVideo': offerToReceive
        }
    });
}

function hangupAndNotify() {
    hangup();
    sendMessage({
        messageType: "HANG_UP"
    });
}

function hangup() {
    callButton.disabled = false;
    hangupButton.disabled = true;
    if (pc != null)
        pc.close();
    pc = null;
}

function handleSocketMessage(message) {

    var msg = JSON.parse(message.body);

    switch (msg.messageType) {
        case 'USERS_IN_ROOM_CHANGE':
            handleUsersInRoomChangeMessage(msg);
            break;
        case 'SDP_OFFER':
            handleSDPOfferMessage(msg);
            break;
        case 'OFFER_ANSWER':
            handleOfferAnswerMessage(msg);
            break;
        case 'ICE_CANDIDATES':
            handleICECandidatesMessage(msg);
            break;
        case 'HANG_UP':
            hangup();
            break;
    }
}

function handleUsersInRoomChangeMessage(msg) {
    if (pc == null) { //нет активного подключения
        if (msg.users.length > 1) {
            callButton.disabled = false;
        } else {
            callButton.disabled = true;
        }
    } else {
        hangup();
    }
    usersList.innerHTML = msg.users.join(',');
}

function handleSDPOfferMessage(msg) {
    callButton.disabled = true;
    hangupButton.disabled = false;
    initPeerConnection();
    pc.setRemoteDescription(new RTCSessionDescription(msg.data));
    pc.createAnswer(function (description) {
        pc.setLocalDescription(description);
        sendMessage({
            messageType: 'OFFER_ANSWER',
            data: description
        })
    }, function (error) {
        console.log(error)
    }, {
        'mandatory': {
            'OfferToReceiveAudio': offerToReceive,
            'OfferToReceiveVideo': offerToReceive
        }
    });
}

function handleOfferAnswerMessage(msg) {
    console.log("answer");
    console.log(msg.data.sdp);
    pc.setRemoteDescription(new RTCSessionDescription(msg.data));
}

function handleICECandidatesMessage(msg) {
    msg.data.forEach(function (item) {
        pc.addIceCandidate(new RTCIceCandidate({
            sdpMLineIndex: item.label,
            candidate: item.candidate
        }));
    });
}

function sendMessage(message) {
    stompClient.send("/socket/message-consumer", {}, JSON.stringify(message));
}