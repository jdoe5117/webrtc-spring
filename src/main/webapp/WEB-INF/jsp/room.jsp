<%@page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8" %>

<!DOCTYPE html>
<html>
<head>
    <title>Chat #${roomId}</title>
    <link href="/webjars/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="/webjars/sockjs-client/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/stomp.min.js"></script>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
</head>
<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable
    Javascript and reload this page!</h2></noscript>

<div class="container">
    <input id="roomId" hidden value="${roomId}">
    <div class="row">
        <h4>Пользователи в этой комнате <span class="label" id="users-list"></span></h4>
    </div>
    <div class="row">
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="disableBundling">
            <label class="form-check-label" for="disableBundling">disableBundling</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-2">
            <button id="callButton" disabled class="btn btn-primary">Инициировать вызов</button>
        </div>
        <div class="col-md-4 col-xs-2">
            <button id="hangupButton" disabled class="btn btn-primary">Остановить вызов</button>
        </div>
        <div class="col-md-4 col-xs-2">
            <button id="addStream" class="btn btn-primary">Add stream</button>
        </div>
        <div class="col-md-4 col-xs-2">
            <button id="removeStreams" class="btn btn-primary">removeStreams</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <video id="yourvid" muted autoplay width="100%" height="100%"></video>
        </div>
        <div class="col-md-6">
            <video id="partnervid0" autoplay width="100%" height="100%"></video>
        </div>
        <div class="col-md-6">
            <video id="partnervid1" autoplay width="100%" height="100%"></video>
        </div>
        <div class="col-md-6">
            <video id="partnervid2" autoplay width="100%" height="100%"></video>
        </div>
    </div>
</div>

<script src="/js/app.js"></script>
</body>
</html>