<!DOCTYPE html>
<html>
<head>
    <title>Choose chat room</title>
    <link href="/webjars/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <form method="get" action="/room">
        <div class="form-group row">
            <label for="room-id-input" class="col-sm-2 col-form-label">Enter room number</label>
            <div class="col-sm-4">
                <input class="form-control" id="room-id-input" name="roomId">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">JOIN</button>
        </div>
    </form>
</div>
</body>
</html>
