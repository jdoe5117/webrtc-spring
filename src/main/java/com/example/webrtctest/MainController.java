package com.example.webrtctest;

import com.example.webrtctest.dtos.messages.AbstractSocketMessage;
import com.example.webrtctest.dtos.messages.UsersInRoomChangeSocketMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

@Controller
public class MainController {

    @Autowired
    private RoomsRepository roomsRepository;

    @Autowired
    private SimpMessagingTemplateDecorator template;

    @RequestMapping("/")
    public String welcome() {
        return "index";
    }

    /**
     * Перенаправляет пользователя на страницу указанной комнаты
     */
    @GetMapping("room")
    public String getRoomView(@RequestParam("roomId") String id, Model model) {
        model.addAttribute("roomId", id);
        return "room";
    }

    /**
     * Метод обрабатывающий подписку нового пользователя, возвращает UsersInRoomChangeSocketMessage
     * При отправке сообщения через SimpMessagingTemplate сразу после получения SessionSubscribeEvent
     * возникает состояние гонки и сообщение может быть отправлено раньше чем подписка
     * будет добавлена в DefaultSubscriptionRegistry. В таком случае оно будет потеряно.
     * SessionSubscribedEvent пока не реализован: https://jira.spring.io/browse/SPR-12844
     */
    @SubscribeMapping("/socket-room/{roomId}")
    public UsersInRoomChangeSocketMessage subscribeHandler(@DestinationVariable("roomId") String roomId, SimpMessageHeaderAccessor headerAccessor){
        headerAccessor.getSessionAttributes().put("roomId", roomId);
        roomsRepository.newSubscription(roomId, headerAccessor.getSessionId());
        return new UsersInRoomChangeSocketMessage(roomsRepository.getRoomUsers(roomId));
    }

    /**
     * Метод принимающий сообщения передаваемые пользователями через web-сокет.
     * Все полученные сообщения переправляются другим пользователям в той же комнате
     */
    @MessageMapping("/message-consumer")
    public void handleChat(@Payload AbstractSocketMessage message, SimpMessageHeaderAccessor headerAccessor) throws JsonProcessingException {
        String roomId = (String) headerAccessor.getSessionAttributes().get("roomId");
        Set<String> usersInThisRoom = roomsRepository.getRoomUsers(roomId);

        for (String session : usersInThisRoom) {
            if (!session.equals(headerAccessor.getSessionId())) {
                template.convertAndSendToUser(session, "/socket-room/" + roomId, message);
            }
        }
    }
}