package com.example.webrtctest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebRtcHelloWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebRtcHelloWorldApplication.class, args);
    }

    /**
     * Регистрация listener`ов в обьекте RoomsRepository
     */
    @Autowired
    public void configureObservers(RoomsRepository rr, RoomsRepoListener notifyOnRoomUserChangeListener) {
        rr.addListener(notifyOnRoomUserChangeListener);
    }
}
