package com.example.webrtctest.dtos.messages;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Сообщение содержащее SDP сообщение отправляемое в ответ пользователю приславшему SDP Offer
 */
public class OfferAnswerSocketMessage extends AbstractSocketMessage {

    private final JsonNode data;

    public OfferAnswerSocketMessage(JsonNode data) {
        super(MessageType.OFFER_ANSWER);
        this.data = data;
    }

    public JsonNode getData() {
        return data;
    }
}
