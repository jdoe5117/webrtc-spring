package com.example.webrtctest.dtos.messages;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Set;

/**
 * Десериализатор сокет сообщений
 * Используется для десериализации и инстанцианирования обьектов сообщений при получении сообщений в контроллере
 */

@SuppressWarnings("unchecked")
public class AbstractSocketMessageDeserializer extends JsonDeserializer {
    @Override
    public AbstractSocketMessage deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        AbstractSocketMessage.MessageType messageType = AbstractSocketMessage.MessageType.valueOf(node.get("messageType").textValue());
        ObjectMapper mapper = new ObjectMapper();
        switch (messageType) {
            case USERS_IN_ROOM_CHANGE:
                return new UsersInRoomChangeSocketMessage(
                        mapper.readValue(node.get("users").toString(), Set.class)
                );
            case OFFER_ANSWER:
                return new OfferAnswerSocketMessage(node.get("data"));
            case SDP_OFFER:
                return new SDPOfferSocketMessage(node.get("data"));
            case ICE_CANDIDATES:
                return new ICECandidatesSocketMessage(node.get("data"));
            case HANG_UP:
                return new HangUpSocketMessage();
        }
        throw new IOException("Error in deserializing AbstractSocketMessage");
    }
}
