package com.example.webrtctest.dtos.messages;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Сообщение содержашее список обьектов ICE Candidate полученных от STUN сервера
 */
public class ICECandidatesSocketMessage extends AbstractSocketMessage {

    private final JsonNode data;

    public ICECandidatesSocketMessage(JsonNode data) {
        super(MessageType.ICE_CANDIDATES);
        this.data = data;
    }

    public JsonNode getData() {
        return data;
    }
}
