package com.example.webrtctest.dtos.messages;

/**
 * Сообщение сигнализирующее о закрытии WebRTC подключения пользователем
 */
public class HangUpSocketMessage extends AbstractSocketMessage {
    public HangUpSocketMessage() {
        super(MessageType.HANG_UP);
    }
}
