package com.example.webrtctest.dtos.messages;

import java.util.Set;

/**
 * Сообщение сигнализируещее пользователя о изменении количества пользователей в комнате
 */
public class UsersInRoomChangeSocketMessage extends AbstractSocketMessage {

    private final Set<String> users;

    public UsersInRoomChangeSocketMessage(Set<String> users) {
        super(MessageType.USERS_IN_ROOM_CHANGE);
        this.users = users;
    }

    public Set<String> getUsers() {
        return users;
    }
}
