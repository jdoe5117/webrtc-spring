package com.example.webrtctest.dtos.messages;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Суперкласс для всех сообщений передаваемых через сокет.
 */

@JsonDeserialize(using = AbstractSocketMessageDeserializer.class)
public abstract class AbstractSocketMessage {

    private MessageType messageType;

    public enum MessageType {
        USERS_IN_ROOM_CHANGE, SDP_OFFER, OFFER_ANSWER, ICE_CANDIDATES, HANG_UP
    }

    public AbstractSocketMessage(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageType getMessageType() {
        return messageType;
    }
}
