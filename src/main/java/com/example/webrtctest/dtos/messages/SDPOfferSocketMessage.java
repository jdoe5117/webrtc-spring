package com.example.webrtctest.dtos.messages;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Сообщение передающее обьект SDP сообщения от пользователя инициирующего создание WebRTC подключения другому пользователю
 */
public class SDPOfferSocketMessage extends AbstractSocketMessage {

    private final JsonNode data;

    public SDPOfferSocketMessage(JsonNode data) {
        super(MessageType.SDP_OFFER);
        this.data = data;
    }

    public JsonNode getData() {
        return data;
    }
}
