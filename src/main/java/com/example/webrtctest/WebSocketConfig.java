package com.example.webrtctest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    RoomsRepository roomsRepository;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/socket-room");
        config.setApplicationDestinationPrefixes("/socket", "/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/socket-endpoint")
                .withSockJS();
    }

    /**
     * При дисконнекте убираем пользователя из базы комнат
     */
    @Bean
    public ApplicationListener<SessionDisconnectEvent> socketSessionDisconnectListener() {
        return event -> {
            StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
            roomsRepository.unsubscribe((String) headerAccessor.getSessionAttributes().get("roomId"), headerAccessor.getSessionId());
        };
    }

    /**
     * При отписке убираем пользователя из базы комнат
     */
    @Bean
    public ApplicationListener<SessionUnsubscribeEvent> socketSessionUnsubscribeListener() {
        return event -> {
            StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
            roomsRepository.unsubscribe((String) headerAccessor.getSessionAttributes().get("roomId"), headerAccessor.getSessionId());
        };
    }
}