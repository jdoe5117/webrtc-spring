package com.example.webrtctest;

import java.util.Set;

public interface RoomsRepoListener {
    enum RoomRepoChangeEventType {
        SUBSCRIBE, UNSUBSCRIBE;
    }

    void handleEvent(String roomId, Set<String> users, String initiatorSessionId, RoomRepoChangeEventType event);
}
