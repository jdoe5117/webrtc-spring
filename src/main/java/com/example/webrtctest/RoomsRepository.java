package com.example.webrtctest;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Репозиторий хранящий список сессий пользователей в комнатах
 */
@Component
public class RoomsRepository {

    private Map<String, Set<String>> rooms = new ConcurrentHashMap<>();
    private List<RoomsRepoListener> listeners = new ArrayList<>();

    /**
     * Обработчик добавления нового пользователя в указанную комнату
     * В случае отсутствия указанной комнаты она будет создана
     */
    public void newSubscription(String roomId, String sessionId) {
        rooms.computeIfAbsent(roomId, s -> ConcurrentHashMap.newKeySet()).add(sessionId);
        notifyListeners(roomId, sessionId, RoomsRepoListener.RoomRepoChangeEventType.SUBSCRIBE);
    }

    /**
     * Обработчик выхода пользователя из комнаты
     * Идентификатор сессии будет удален их указанной комнаты
     * Если он был единственным, то комната будет удалена
     */
    public void unsubscribe(String roomId, String sessionId) {
        Set<String> users = rooms.getOrDefault(roomId, Collections.emptySet());
        if (users.remove(sessionId)) {
            if (users.isEmpty()) {
                rooms.remove(roomId);
            } else {
                notifyListeners(roomId, sessionId, RoomsRepoListener.RoomRepoChangeEventType.UNSUBSCRIBE);
            }
        }
    }

    /**
     * Метод возвращает список пользовательских сессий в указанной комнате
     */
    public Set<String> getRoomUsers(String roomId) {
        Set<String> users = rooms.getOrDefault(roomId, Collections.emptySet());
        if (users.isEmpty())
            return Collections.emptySet();

        return new HashSet<>(users);
    }

    /**
     * Добавление listener`а изменений обьекта комнаты
     */
    public void addListener(RoomsRepoListener listener) {
        listeners.add(listener);
    }

    /**
     * Метод оповещающий подписанных listener`ов о изменении обьекта комнаты
     */
    private void notifyListeners(String roomId, String initiatorSessionId, RoomsRepoListener.RoomRepoChangeEventType eventType) {
        for (RoomsRepoListener listener : listeners) {
            listener.handleEvent(roomId, getRoomUsers(roomId), initiatorSessionId, eventType);
        }
    }
}
