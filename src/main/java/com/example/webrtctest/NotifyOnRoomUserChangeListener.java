package com.example.webrtctest;

import com.example.webrtctest.dtos.messages.AbstractSocketMessage;
import com.example.webrtctest.dtos.messages.UsersInRoomChangeSocketMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Listener получающий изменения обьектов комнат в RoomsRepository и извещающий остальных пользователей комнаты о возможности инициации WebRTC подключения
 * Если состояние комнаты было изменено в результате новой подписки то отправляем сообщение всем кроме подписавшегося, иначе отправляем всем
 * Подписавшийся получит сообщение в ответе сообщения подписки в MainController.subscribeHandler
 */
@Component(value = "notifyOnRoomUserChangeListener")
public class NotifyOnRoomUserChangeListener implements RoomsRepoListener {

    @Autowired
    private SimpMessagingTemplateDecorator template;

    @Override
    public void handleEvent(String roomId, Set<String> users, String initiatorSessionId, RoomRepoChangeEventType event) {
        AbstractSocketMessage message = new UsersInRoomChangeSocketMessage(users);

        users.stream()
                .filter(s -> !(event.equals(RoomRepoChangeEventType.SUBSCRIBE) && s.equals(initiatorSessionId)))
                .forEach(s -> template.convertAndSendToUser(s, "/socket-room/" + roomId, message));

    }
}
