package com.example.webrtctest;

import com.example.webrtctest.dtos.messages.AbstractSocketMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * Обертка для обьекта SimpMessagingTemplate
 * SimpMessagingTemplate используется для отправки сообщений по web-сокету
 * Обертка создана для типизации отправляемых сообщений
 */
@Component
public class SimpMessagingTemplateDecorator {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    public void convertAndSendToUser(String user, String destination, AbstractSocketMessage message) {
        simpMessagingTemplate.convertAndSendToUser(user, destination, message, createHeaders(user));
    }

    private MessageHeaders createHeaders(String sessionId) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        return headerAccessor.getMessageHeaders();
    }

}
